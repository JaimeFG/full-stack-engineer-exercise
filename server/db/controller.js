var promise = require('bluebird');
var connection = require('./connection/dev');
var Files = require('../src/lib/Files');


var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);

var db = pgp(connection);

// add query functions

module.exports = {
  getAllFiles: getAllFiles,
  getFile: getFile,
  createFile: createFile
};

/**
 * Get all files in DB
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function getAllFiles(req, res, next) {
  db.any('select * from Files')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ' + data.length + ' files'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}
/**
 * Get one file by ID
 * @param {} req 
 * @param {*} res 
 * @param {*} next 
 */
function getFile(req, res, next) {
  var itemId = parseInt(req.params.id);

  db.one('select * from Files where id = $1', itemId)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ONE File'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}
/**
 * Create a new file in DB
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function createFile(req, res, next) {

  var file = req.file;

  Files.parseFile(file)
    .then((data) => {
      db.one('INSERT INTO Files(name, size)' +
        'VALUES(${name}, ${size}) RETURNING id',
        data)
        .then(function (result) {

          db.task(function (t) {
            var queries = [];
            data.data.forEach(function (item) {
              // querying against the task protocol:
              let rec = {
                id: result.id,
                value1: item[1],
                value2: item[2]
              }
              queries.push(t.none('INSERT INTO Record(fileID, value1, value2) VALUES(${id}, ${value1}, ${value2})', rec));
            });
            return t.batch(queries); // settles all queries;
          })

          res.status(200)
            .json({
              status: 'success',
              message: 'Inserted one file',
            });
        })
        .catch(function (err) {
          return next(err);
        });
    })
    .catch((err) => {
      res.status(400).send(err)
    });



}
