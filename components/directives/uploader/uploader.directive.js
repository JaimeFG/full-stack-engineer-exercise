; (function () {

  'use strict';

  /**
   * Directive to upload files
   * @author Jaime
   * @ngdoc  Directive
   *
   * @example
   * <uploader><uploader/>
   *
   */
  angular
    .module('boilerplate')
    .directive('uploader', uploader);

  uploader.$inject = ['Files'];

  function uploader(Files) {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      replace: false,
      scope: {
        file: '=',
        loadFile: '=',
        uploadFile: '='
      },
      link: directiveLink,
      templateUrl: 'components/directives/uploader/uploader.tpl.html'
    };

    function directiveLink(scope, elem, attrs, ctrl) {
      scope.load = load;
      scope.upload = upload;
    }

    function load(item) {
      var self = this;
      var file = item.files[0];
      Files.setFile(file)
        .then(function(result) {
          self.loadFile();
          console.log('Cargado fichero...', file.name, 'Tamaño: ', file.size);
        })
        .catch(function(err) {
          alert(err.msg);
          console.log('Error', err.msg);
        });
    }

    function upload() {
      var self = this;

      self.uploadFile();
    }
    return directiveDefinitionObject;
  }

})();
